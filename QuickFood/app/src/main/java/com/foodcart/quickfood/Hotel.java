package com.foodcart.quickfood;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anvar on 12/17/2017.
 */

public class Hotel {

    public String username,password;
    public boolean isApproved;
    public List<Item> item = new ArrayList<Item>();
    public HotelDetails details;
    public HotelLocation location;
    public String mobileNumber;
    public  String address,area,zipCode;
    public Hotel()
    {

    }

    public Hotel(String username,String password,boolean isApproved,HotelDetails details,List<Item> item ,HotelLocation location)
    {
        this.username = username;
        this.password =password;
        this.isApproved = isApproved;
        this.details =details;
        this.item = item;
        this.location = location;
    }
    public Hotel(String username,String password,boolean isApproved,HotelDetails details,HotelLocation location)
    {
        this.username = username;
        this.password =password;
        this.isApproved = isApproved;
        this.details =details;
        this.location = location;
    }
//    public Hotel(String mobileNumber, String address, String area, String zipCode,double lon,double lat)
//    {
//        this.mobileNumber = mobileNumber;
//        this.address =address;
//        this.area = area;
//        this.zipCode =zipCode;
//        this.lon = lon;
//        this.lat=lat;
//    }
//    public Hotel(String mobileNumber, String address,double lat,double lon)
//    {
//        this.mobileNumber = mobileNumber;
//        this.address =address;
//        this.lon = lon;
//        this.lat=lat;
//    }

}
class HotelLocation
{
    public double latitude,longitude;

    public HotelLocation()
    {

    }
    public HotelLocation(double latitude,double longitude)
    {
        this.latitude = latitude;
        this.longitude =longitude;
    }
}

class HotelDetails
{
    public String mobileNumber,name,ownerName,emailId,address;
    public HotelDetails()
    {

    }
    public HotelDetails(String name, String ownerName,String emailId,String address,String mobileNumber)
    {
        this.name = name;
        this.ownerName =ownerName;
        this.emailId = emailId;
        this.address = address;
        this.mobileNumber = mobileNumber;
    }
}
class Item
{
    public String name,type;
    public float cost;
    public boolean isVeg;
    public Item()
    {

    }
    public Item(String name,String type, float cost,boolean isVeg)
    {
        this.name = name;
        this.type =type;
        this.cost = cost;
        this.isVeg =isVeg;

    }

}
