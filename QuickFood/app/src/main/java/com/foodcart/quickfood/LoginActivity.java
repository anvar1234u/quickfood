package com.foodcart.quickfood;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;

import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class LoginActivity extends ActionBarActivity implements View.OnClickListener {

    //STATES

    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_AUTOLOGIN_FAILED = 2;
    private static final int STATE_AUTO_LOGIN = 3;
    private static final int STATE_NORMAL_LOGIN = 4;
    private static final int STATE_SIGN_UP = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;

    public static boolean isLogout=false;
    public static User currentUser;
    public static boolean isOpenPlacePicker=false;

    private static final int MY_PERMISSION_REQUEST_LOCATION=1;
    private boolean mVerificationInProgress = false;
    private String mVerificationId;

    ProgressDialog dialog;

    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private FirebaseAuth mAuth;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference ref;

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    TextView tvOtpSent;
    EditText etName,etPhoneNumber,etOTP;

    String sName,sPhoneNumber,sOtp;
    public static String userId="";

    Button btVerify,btResend,btNext,bImHungy,bLogin,bSignUp;
    LinearLayout loginButtonPanel,loginPanel,signUP;
    RelativeLayout otpPanel;

    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
//        database = FirebaseDatabase.getInstance();
        ref = database.getReference();
//        ref = database.getReference("server/saving-data/fireblog");
        dialog=new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        OnPhoneVerificationFinish();
        mAuth = FirebaseAuth.getInstance();

        btVerify = (Button) findViewById(R.id.bVerify);
        btNext = (Button) findViewById(R.id.bNext);
        btResend = (Button) findViewById(R.id.bResend);
        bImHungy =(Button)findViewById(R.id.bImHungry);
        bLogin = (Button) findViewById(R.id.bLogin);
        bSignUp = (Button)findViewById(R.id.bNextSignUp);

        etName = (EditText) findViewById(R.id.etName);
        etPhoneNumber = (EditText) findViewById(R.id.etPhoneNumber);

        tvOtpSent = (TextView)findViewById(R.id.tvOTP);


        etOTP = (EditText) findViewById(R.id.etOTP);

        loginPanel =(LinearLayout) findViewById(R.id.pLoginEntry);
        signUP =(LinearLayout) findViewById(R.id.pSignUp);
        loginButtonPanel =(LinearLayout) findViewById(R.id.pLoginButton);
        otpPanel = (RelativeLayout)findViewById(R.id.otpEntry);

        btVerify.setOnClickListener(this);
        btResend.setOnClickListener(this);
        btNext.setOnClickListener(this);
        bImHungy.setOnClickListener(this);
        bLogin.setOnClickListener(this);
        bSignUp.setOnClickListener(this);
//        FirebaseUser currentUser = mAuth.getCurrentUser();
//        updateUI(currentUser);
//        txt = (TextView) findViewById(R.id.msg);
//        text = (EditText) findViewById(R.id.gd);
//        handler.postDelayed(run, 1000);
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        tvOtpSent.setText("");
        dialog.show();
        if(isLogout)
        {
            UpdateUI(STATE_AUTOLOGIN_FAILED);
        }
        else
        {
            AutoLogin(currentUser);
        }
//        DetectLocation();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode)
        {
            case MY_PERMISSION_REQUEST_LOCATION:{
                if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    if(ContextCompat.checkSelfPermission(LoginActivity.this,
                            Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                    {
                        LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
                        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        try
                        {
                            String[] curAdrdress;
                            curAdrdress = CurrentLocation(location.getLatitude(),location.getLongitude());
                            etName.setText(curAdrdress[0]);
//                            etName.setText(CurrentLocation(location.getLatitude(),location.getLongitude()));
                        }catch (Exception ex)
                        {
                            ex.printStackTrace();
                            Toast.makeText(LoginActivity.this,"Not Found", Toast.LENGTH_SHORT).show();
                        }
                    }
                }else
                {
                    Toast.makeText(LoginActivity.this,"No permission granted", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public String[] CurrentLocation(double lat, double lon)
    {
        String[] currentCity= new String[3];
        Geocoder geocoder = new Geocoder(LoginActivity.this, Locale.getDefault());
        List<Address> addressList;
        try
        {
            addressList = geocoder.getFromLocation(lat,lon,1);
            if(addressList.size() >0)
            {
                currentCity[0] = addressList.get(0).getSubThoroughfare();
                currentCity[1] = addressList.get(0).getSubLocality();
                currentCity[2] = addressList.get(0).getPostalCode();
            }

        }catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return currentCity;
    }
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.bVerify:
                if(etPhoneNumber.getText().length() != 10)
                {
                    Toast.makeText(LoginActivity.this,"Enter valid phone number", Toast.LENGTH_LONG).show();
                }
                else
                {
                    sPhoneNumber = etPhoneNumber.getText().toString();
                    UpdateUI(STATE_INITIALIZED);
                    dialog.show();
                    PhoneAuthProvider.getInstance().verifyPhoneNumber(
                            sPhoneNumber,
                            60,
                            TimeUnit.SECONDS,
                            this,
                            mCallbacks
                    );
                }

                break;
            case R.id.bResend:
                dialog.show();
                resendVerificationCode(sPhoneNumber,mResendToken);
                break;
            case R.id.bNext:
                Log.d("Verification", "bNext:");
                if(etOTP.getText().length() != 6)
                {
                    Toast.makeText(LoginActivity.this,"Invalid OTP", Toast.LENGTH_LONG).show();
                }
                else
                {
                    sOtp = etOTP.getText().toString();
                    verifyPhoneNumberWithCode(mVerificationId,sOtp);
                }

                break;
            case R.id.bLogin:
                loginButtonPanel.setVisibility(View.GONE);
                loginPanel.setVisibility(View.VISIBLE);

                etPhoneNumber.requestFocus();
                break;
            case R.id.bImHungry:
                OpenMapActivity();
//                startActivity(new Intent(LoginActivity.this,MainActivity.class));
                break;
            case R.id.bNextSignUp:
                sName = etName.getText().toString();
                if(sName !="")
                {
                    SetUserDetails(sPhoneNumber,sName);
                }
                else
                {
                    Toast.makeText(LoginActivity.this,"Enter Valid Name", Toast.LENGTH_SHORT).show();
                    etName.requestFocus();
                }
                break;
        }
    }
    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        Log.d("Verification", "verifyPhoneNumberWithCode:");
        Log.d("Verification", "verifyPhoneNumberWithCode:" + credential.getSmsCode());
        // [END verify_with_code]
        signInWithPhoneAuthCredential(credential);
    }
    Context context;
    public boolean isLocationServiceEnabled(){
        context=this;
        LocationManager locationManager = null;
        boolean gps_enabled= false,network_enabled = false;

        if(locationManager ==null)
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        try{
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }catch(Exception ex){
            //do nothing...
        }

        try{
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }catch(Exception ex){
            //do nothing...
        }

        return gps_enabled || network_enabled;

    }

    private void DetectLocation()
    {
        if(ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)){
                ActivityCompat.requestPermissions(LoginActivity.this,
                        new String[]{ Manifest.permission.ACCESS_COARSE_LOCATION},MY_PERMISSION_REQUEST_LOCATION);
            }
            else
            {
                ActivityCompat.requestPermissions(LoginActivity.this,
                        new String[]{ Manifest.permission.ACCESS_COARSE_LOCATION},MY_PERMISSION_REQUEST_LOCATION);
            }
        }
        else
        {
            LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            try
            {
                if(location != null) {
                    String[] curAdrdress;
                    curAdrdress = CurrentLocation(location.getLatitude(), location.getLongitude());
                    etName.setText(curAdrdress[0]);
                }
//                etName.setText(CurrentLocation(location.getLatitude(),location.getLongitude()));
            }catch (Exception ex)
            {
                ex.printStackTrace();
                Toast.makeText(LoginActivity.this,"Not Found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dialog.dismiss();
    }
    //sign up user user
    private void SetUserDetails(String sPhoneNumber, String sName, double lat, double lon)
    {
        User user =new User(sPhoneNumber,sName);//0.0,0.0);
        currentUser = user;
//        userId = uId;
        ref.child("users").child(userId).setValue(user);

//        Map<String, User> users = new HashMap<>();
//        users.put(uId, new User(sPhoneNumber,sName,sArea,sZip ));
        OpenMapActivity();
//        startActivity(new Intent(LoginActivity.this,MainActivity.class));
    }
    private void SetUserDetails(String sPhoneNumber, String sName)
    {
        User user =new User(sPhoneNumber,sName);//0.0,0.0);
        currentUser = user;
//        userId = uId;
        ref.child("users").child(userId).setValue(user);

//        Map<String, User> users = new HashMap<>();
//        users.put(uId, new User(sPhoneNumber,sName,sArea,sZip ));
        OpenMapActivity();
//        startActivity(new Intent(LoginActivity.this,MainActivity.class));
    }
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {

        sName =etName.getText().toString();
        dialog.show();
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        dialog.hide();
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("SignIn", "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();
                            userId = user.getUid();
                            DatabaseReference tempRef = ref.child("users").child(user.getUid()).getRef();
                            FetchUserDetailsFromFirebseDatabase(tempRef,STATE_NORMAL_LOGIN);

                            Toast.makeText(LoginActivity.this,"verification Success "+user.getUid(), Toast.LENGTH_LONG).show();

                            // ...
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w("SignIn", "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                            }
                        }
                    }
                });
    }
    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }
    public void OnPhoneVerificationFinish()
    {
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks()
        {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                Log.d("Verification", "onVerificationCompleted:" + phoneAuthCredential);
                etOTP.setText(phoneAuthCredential.getSmsCode());
                Toast.makeText(LoginActivity.this,"verification Success", Toast.LENGTH_LONG).show();
                dialog.hide();
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                e.printStackTrace();
                dialog.hide();
                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // [START_EXCLUDE]
                    Toast.makeText(LoginActivity.this,"Invalid phone number", Toast.LENGTH_LONG).show();

                    // [END_EXCLUDE]
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // [START_EXCLUDE]
                    Toast.makeText(LoginActivity.this,"Quota exceeded.", Toast.LENGTH_LONG).show();

                    // [END_EXCLUDE]
                }

            }

            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
//                super.onCodeSent(s, forceResendingToken);
                mVerificationId = verificationId;
                mResendToken = forceResendingToken;
                dialog.hide();
//                Toast.makeText(LoginActivity.this,"Code sent", Toast.LENGTH_LONG).show();
                tvOtpSent.setText("An OTP is sent to "+sPhoneNumber+". Please enter the OTP to continue");
            }
        };
    }
    void UpdateUI(int STATE)
    {
        switch (STATE)
        {
            case STATE_INITIALIZED:
                otpPanel.setVisibility(View.VISIBLE);
                etOTP.requestFocus();
                btNext.setVisibility(View.VISIBLE);
                btVerify.setVisibility(View.GONE);
                break;
            case STATE_AUTOLOGIN_FAILED:
                loginButtonPanel.setVisibility(View.VISIBLE);
                break;
            case STATE_SIGN_UP:
                signUP.setVisibility(View.VISIBLE);
                loginPanel.setVisibility(View.GONE);
                break;
        }

    }
    private void AutoLogin(FirebaseUser currentUser)
    {
        if (currentUser != null) {
            Toast.makeText(LoginActivity.this,"Already loged in", Toast.LENGTH_LONG).show();
            DatabaseReference tempRef = ref.child("users").child(currentUser.getUid()).getRef();
            FetchUserDetailsFromFirebseDatabase(tempRef,STATE_AUTO_LOGIN);
//            ref.child("users").child(uId).setValue(user);
            isOpenPlacePicker=false;
        } else {
            Toast.makeText(LoginActivity.this,"not loged in", Toast.LENGTH_LONG).show();
            dialog.hide();
            UpdateUI(STATE_AUTOLOGIN_FAILED);
            isOpenPlacePicker=true;
//            if(isLocationServiceEnabled()) {
//                DetectLocation();
//            }
        }

    }

    private void FetchUserDetailsFromFirebseDatabase(DatabaseReference tempRef, int state) {
        tempRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                switch (state)
                {
                    case STATE_AUTO_LOGIN:
                        currentUser = dataSnapshot.getValue(User.class);
                        OpenMapActivity();
//                        startActivity(new Intent(LoginActivity.this,MainActivity.class));
                        break;
                    case STATE_NORMAL_LOGIN:
                        currentUser = dataSnapshot.getValue(User.class);
                        if(currentUser != null)
                        {
                            OpenMapActivity();
//                            startActivity(new Intent(LoginActivity.this,MainActivity.class));
                        }
                        else
                        {
                            UpdateUI(STATE_SIGN_UP);
                        }
//                        SetUserDetails(dataSnapshot.getUid(),sPhoneNumber,sName,sArea,sZip);

                        break;
                }

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                dialog.hide();
            }

        });
    }
    private void OpenMapActivity()
    {
        finish();
        startActivity(new Intent(LoginActivity.this,MainActivity.class));
    }
//    Runnable run = new Runnable() {
//        @Override
//        public void run() {
//            getCode();
//        }
//    };

//    public void getCode() {
//        StringBuilder smsBuilder = new StringBuilder();
//        final String SMS_URI_INBOX = "content://sms/inbox";
//        final String SMS_URI_ALL = "content://sms/";
//        try {
//            Uri uri = Uri.parse(SMS_URI_INBOX);
//            String[] projection = new String[]{"_id", "address", "person", "body", "date", "type"};
//            Cursor cur = getContentResolver().query(uri, projection, "address LIKE '%MyTsky'", null, "date desc limit 1");
//            if (cur.moveToFirst()) {
//                int index_Address = cur.getColumnIndex("address");
//                int index_Person = cur.getColumnIndex("person");
//                int index_Body = cur.getColumnIndex("body");
//                int index_Date = cur.getColumnIndex("date");
//                int index_Type = cur.getColumnIndex("type");
//                do {
//                    String strAddress = cur.getString(index_Address);
//                    int intPerson = cur.getInt(index_Person);
//                    String strbody = cur.getString(index_Body);
//                    long longDate = cur.getLong(index_Date);
//                    int int_Type = cur.getInt(index_Type);
//
//                    smsBuilder.append("[ ");
//                    smsBuilder.append(strAddress + ", ");
//                    smsBuilder.append(intPerson + ", ");
//                    smsBuilder.append(strbody + ", ");
//                    smsBuilder.append(longDate + ", ");
//                    smsBuilder.append(int_Type);
//                    smsBuilder.append(" ]\n\n");
//
//                    String str = strbody;
//                    Pattern pattern = Pattern.compile("\\w+([0-9])");
//                    Matcher matcher = pattern.matcher(str);
//                    for (int i = 0; i < matcher.groupCount(); i++) {
//                        matcher.find();
//                        Log.e("No....--", matcher.group());
//                        if (matcher.group().equals("")) {
//                            handler.postDelayed(run, 7000);
//                        } else {
//                            text.setText(matcher.group());
//                        }
//                    }
//                } while (cur.moveToNext());
//
//                if (!cur.isClosed()) {
//                    cur.close();
//                    cur = null;
//                }
//            } else {
//                smsBuilder.append("no result!");
//            } // end if
//
//            Log.e("sms", smsBuilder.toString());
//            txt.setText(smsBuilder.toString());
//        } catch (SQLiteException ex) {
//            Log.d("SQLiteException", ex.getMessage());
//        }
//    }
}
