package com.foodcart.quickfood;

import android.Manifest;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    TextView placeNameText;
    TextView placeAddressText;
    WebView attributionText;
    Button getPlaceButton,addHotel;

    ViewPager viewPager;

    private final static String IS_GET_LOCATION ="GotLocation";
    private final static int MY_PERMISSION_FINE_LOCATION = 101;
    private final static int PLACE_PICKER_REQUEST = 1;
    private final static int FETCH_HOTEL_DETAILS = 2;
    private final static int FETCH_USER_DETAILS = 3;
    private final static int PLACE_PICKER_REQUEST_FOR_HOTEL = 4;
    private static final int MY_PERMISSION_REQUEST_LOCATION=1;


    private static final float MINIMUM_DISTANCE_BETWEEN_HOTEL_AND_CUSTOMER=5000;

    ProgressDialog dialog;

    public static double lat;
    public static double lon;
    int uid=0;

    UserLocalStore userLocalStore;

    List<Hotel> availabelHotels=null;

    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    FirebaseDatabase hotelDatabase;

    DatabaseReference ref,hotelRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dialog=new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);


        ref = database.getReference();

        availabelHotels= new ArrayList<Hotel>();
        ConnectToHotelDB();
        requestPermission();

        placeNameText = (TextView) findViewById(R.id.tvPlaceName);
        placeAddressText = (TextView) findViewById(R.id.tvPlaceAddress);
        attributionText = (WebView) findViewById(R.id.wvAttribution);
        getPlaceButton = (Button) findViewById(R.id.btGetPlace);
        viewPager = (ViewPager)findViewById(R.id.viewPager);

        SlideImageHandler();


        getPlaceButton.setOnClickListener(this);
        addHotel = (Button) findViewById(R.id.btAddHotel);
        addHotel.setOnClickListener(this);


        userLocalStore = new UserLocalStore(this);


    }
    public void onClick(View v) {
        switch(v.getId()) {

            case R.id.btGetPlace:
                openPlacePicker();
            break;
            case R.id.btAddHotel:
                openPlacePickerForHotel();
                break;
        }
    }

    private void openPlacePicker()
    {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            Intent intent = builder.build(MainActivity.this);
            startActivityForResult(intent, PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    //To be Deleted
    private void openPlacePickerForHotel()
    {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            Intent intent = builder.build(MainActivity.this);
            startActivityForResult(intent, PLACE_PICKER_REQUEST_FOR_HOTEL);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void requestPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
            }
        }
        else
        {
            if(LoginActivity.isOpenPlacePicker)
            {
                openPlacePicker();
            }
            else
            {
                FetchDataFromFirebaseDBUser();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case MY_PERMISSION_FINE_LOCATION:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "This app requires location permissions to be granted", Toast.LENGTH_LONG).show();
                    finish();
                }
                else
                {
                    if(LoginActivity.isOpenPlacePicker)
                    {
                        openPlacePicker();
                    }
                    else
                    {
                        FetchDataFromFirebaseDBUser();
                    }
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode)
        {
            case PLACE_PICKER_REQUEST:
                if (resultCode == RESULT_OK){
                    onPlacePickDone(data);
                }
                else
                {
                    DetectLocation();
                }
                break;
            case PLACE_PICKER_REQUEST_FOR_HOTEL:
                Place place = PlacePicker.getPlace(MainActivity.this, data);
                LatLng latLng = place.getLatLng();
//                String address = CurrentLocation(latLng.latitude,latLng.longitude);
                placeNameText.setText(place.getName());
                placeAddressText.setText(place.getAddress());
                try {
                    Item item1 = new Item("VegKuruma","Kuruma",75.00f,true);
                    Item item2 = new Item("Chicken Kabab F","Kabab",100.00f,false);
                    List<Item> items = new ArrayList<Item>();
                    items.add(item1);
                    items.add(item2);

                    HotelDetails details = new HotelDetails("xxxx","yyyy","xxx@yyy.com",place.getAddress().toString(),"98565689689");
                    HotelLocation location = new HotelLocation(latLng.latitude,latLng.longitude);
                    uid++;
                    Hotel hotel = new Hotel("hotel"+uid,"hotel"+uid,true,details,items,location);
                    hotelRef.child("hotel").child("hotel"+uid).setValue(hotel);

                }catch (Exception ex)
                {
                    ex.printStackTrace();
                }

                break;
        }
    }

    private void onPlacePickDone(Intent data)
    {
        Place place = PlacePicker.getPlace(MainActivity.this, data);
        LatLng latLng = place.getLatLng();
        lat = latLng.latitude;
        lon = latLng.longitude;
        placeNameText.setText(place.getName());
        placeAddressText.setText(place.getAddress());
        if(LoginActivity.userId !="")
        {
            User user = new User(LoginActivity.currentUser.mobileNumber,LoginActivity.currentUser.name,lat,lon);
            ref.child("users").child(LoginActivity.userId).setValue(user);
        }
        FetchDataFromFirebaseDBHotel();

//        if (place.getAttributions() == null) {
//            attributionText.loadData("no attribution", "text/html; charset=utf-8", "UFT-8");
//        } else {
//            attributionText.loadData(place.getAttributions().toString(), "text/html; charset=utf-8", "UFT-8");
//        }
    }
    private void ConnectToHotelDB()
    {
        FirebaseOptions options = new FirebaseOptions.Builder()
             .setApplicationId("1:250334252316:android:5d313cecacf5f803") // Required for Analytics.
             .setApiKey("AIzaSyDpjWtPaCfTQEulIISlu0-f10N94A4vDhE") // Required for Auth.
             .setDatabaseUrl("https://quickfoodhotel.firebaseio.com/") // Required for RTDB.
             .build();
        FirebaseApp.initializeApp(this /* Context */, options, "secondary");
        FirebaseApp app = FirebaseApp.getInstance("secondary");
        hotelDatabase = FirebaseDatabase.getInstance(app);
        hotelRef = hotelDatabase.getReference();
//        DatabaseReference tempRef1 = hotelRef.child("hotel").getRef();

    }
    private void FetchDataFromFirebaseDBHotel()
    {
//        dialog.show();
        DatabaseReference tempRef1 = hotelRef.child("hotel").getRef();
        onFetchDataFinshFromFirebseDatabase(FETCH_HOTEL_DETAILS,tempRef1);
    }
    private void FetchDataFromFirebaseDBUser()
    {
//        dialog.show();
        DatabaseReference tempRef1 = ref.child("users").child(LoginActivity.userId).getRef();
        onFetchDataFinshFromFirebseDatabase(FETCH_USER_DETAILS,tempRef1);
    }
    void onFetchDataFinshFromFirebseDatabase(int type,DatabaseReference tempRef)
    {
        tempRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dialog.hide();
                switch (type)
                {
                    case FETCH_HOTEL_DETAILS:
//                        availableHotelLength=0;
                        for (DataSnapshot ds:dataSnapshot.getChildren()) {
                            Hotel hotel = ds.getValue(Hotel.class);


//                            HotelLocation location = hotel.location;

                            Location locationHotel = new Location("");
                            locationHotel.setLatitude(hotel.location.latitude);
                            locationHotel.setLongitude(hotel.location.longitude);

                            Location locationUser = new Location("");
                            locationUser.setLatitude(lat);
                            locationUser.setLongitude(lon);

                            float distance = locationHotel.distanceTo(locationUser);
                            Log.d("Fetch","DISTANCE "+distance);
                            if(distance <= MINIMUM_DISTANCE_BETWEEN_HOTEL_AND_CUSTOMER)
                            {
                                availabelHotels.add(hotel);
                            }

//                            text += hotel.mobileNumber + "  " + hotel.address + " " + hotel.area + " "+hotel.zipCode+"\n";
                        }
                        LoadHomePage();
                        break;
                    case FETCH_USER_DETAILS:
                        User user = dataSnapshot.getValue(User.class);
                        lat = user.lat;
                        lon = user.lon;
                        FetchDataFromFirebaseDBHotel();
                        break;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                dialog.hide();
            }
        });
    }

    private void LoadHomePage()
    {
        if(availabelHotels.size()>0)
        {
            Toast.makeText(MainActivity.this,availabelHotels.size()+" Hotels found", Toast.LENGTH_SHORT).show();
            for (Hotel hotel:availabelHotels) {
                    Log.d("Fetch",hotel.details.address);
            }

        }
          else
        {
            Toast.makeText(MainActivity.this,"No Hotels Found", Toast.LENGTH_SHORT).show();
        }




    }
    public String CurrentLocation(double lat, double lon)
    {
        String currentCity="";
        Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
        List<Address> addressList;
        try
        {
            addressList = geocoder.getFromLocation(lat,lon,1);
            if(addressList.size() >0)
            {
                currentCity = addressList.get(0).getAddressLine(0);
            }

        }catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return currentCity;
    }
    private void DetectLocation()
    {
        if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)){
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{ Manifest.permission.ACCESS_COARSE_LOCATION},MY_PERMISSION_REQUEST_LOCATION);
            }
            else
            {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{ Manifest.permission.ACCESS_COARSE_LOCATION},MY_PERMISSION_REQUEST_LOCATION);
            }
        }
        else
        {
            LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            lat = location.getLatitude();
            lon = location.getLongitude();
            if(LoginActivity.userId !="")
            {
                User user = new User(LoginActivity.currentUser.mobileNumber,LoginActivity.currentUser.name,lat,lon);
                ref.child("users").child(LoginActivity.userId).setValue(user);
            }
            FetchDataFromFirebaseDBHotel();
            try
            {
                if(location != null) {
                    String curAdrdress;
                    curAdrdress = CurrentLocation(location.getLatitude(), location.getLongitude());
                    placeAddressText.setText(curAdrdress);
//                    etName.setText(curAdrdress[0]);
                }
//                etName.setText(CurrentLocation(location.getLatitude(),location.getLongitude()));
            }catch (Exception ex)
            {
                ex.printStackTrace();
                Toast.makeText(MainActivity.this,"Not Found", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void SlideImageHandler()
    {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this);
        viewPager.setAdapter(viewPagerAdapter);
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new MyTimerTask(),2000,4000);

    }

    public class MyTimerTask extends TimerTask{

        @Override
        public void run() {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(viewPager.getCurrentItem() ==0)
                    {
                        viewPager.setCurrentItem(1);
                    }
                    else if(viewPager.getCurrentItem()==1)
                    {
                        viewPager.setCurrentItem(1);
                    }
                    else
                    {
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }


//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        ref = database.getReference();
//
//        FirebaseOptions options = new FirebaseOptions.Builder()
//                .setApplicationId("1:250334252316:android:5d313cecacf5f803") // Required for Analytics.
//                .setApiKey("AIzaSyDpjWtPaCfTQEulIISlu0-f10N94A4vDhE") // Required for Auth.
//                .setDatabaseUrl("https://quickfoodhotel.firebaseio.com/") // Required for RTDB.
//                .build();
//        FirebaseApp.initializeApp(this /* Context */, options, "secondary");
//
//        FirebaseApp app = FirebaseApp.getInstance("secondary");
//        hotelDatabase = FirebaseDatabase.getInstance(app);
//        hotelRef = hotelDatabase.getReference();
//        DatabaseReference tempRef1 = hotelRef.child("hotel").getRef();
////        DatabaseReference tempRef = hotelRef.child("hotel").child("0Z5clnnNQuVq8RwdybWoBqjpd1A3").getRef();
//        FetchDataFromFirebseDatabase(FETCH_HOTEL_DETAILS,tempRef1);
//        tDetails =(TextView)findViewById(R.id.userDetails);
//    }
//    void FetchDataFromFirebseDatabase(int type,DatabaseReference tempRef)
//    {
//        tempRef.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                switch (type)
//                {
//                    case FETCH_HOTEL_DETAILS:
////                        Hotel hotel = dataSnapshot.getValue(Hotel.class);
////                        text = hotel.mobileNumber + "  " + hotel.address + " " + hotel.area + " "+hotel.zipCode;
////                        tDetails.setText(text);
//                        for (DataSnapshot ds:dataSnapshot.getChildren()) {
//                            Hotel hotel = ds.getValue(Hotel.class);
//                            text += hotel.mobileNumber + "  " + hotel.address + " " + hotel.area + " "+hotel.zipCode+"\n";
//                        }
//                        tDetails.setText(text);
//                        break;
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//    }
}
