package com.foodcart.quickfood;

/**
 * Created by anvar on 12/17/2017.
 */

public class User {

    public String mobileNumber;
    public  String name;
    double lon,lat;
    public User()
    {

    }

    public User(String mobileNumber, String name, double lat, double lon)
    {
        this.mobileNumber = mobileNumber;
        this.name =name;
        this.lon = lon;
        this.lat =lat;
    }
    public User(String mobileNumber, String name)
    {
        this.mobileNumber = mobileNumber;
        this.name =name;
    }
}
