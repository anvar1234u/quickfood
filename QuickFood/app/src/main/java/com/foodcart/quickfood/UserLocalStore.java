package com.foodcart.quickfood;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by anvar on 12/24/2017.
 */

public class UserLocalStore {
    public static final String SP_NAME="userDetails";
    SharedPreferences userLocalDatabase;
    public UserLocalStore(Context context){
        userLocalDatabase=context.getSharedPreferences(SP_NAME,0);
    }

    public void SetString(String key, String value)
    {
        SharedPreferences.Editor spEditor=userLocalDatabase.edit();
        spEditor.putString(key,value);
    }

    public String GetString(String key)
    {
        String value;
        value = userLocalDatabase.getString(key,"");
        return value;
    }

    public void SetBoolean(String key, boolean value)
    {
        SharedPreferences.Editor spEditor=userLocalDatabase.edit();
        spEditor.putBoolean(key,value);
    }

    public boolean GetBoolean(String key)
    {
        boolean value;
        value = userLocalDatabase.getBoolean(key,false);
        return value;
    }




    public void clearUserData(){
        SharedPreferences.Editor spEditor=userLocalDatabase.edit();
        spEditor.clear();
        spEditor.commit();
    }
}
